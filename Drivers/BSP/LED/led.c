#include "./BSP/LED/led.h"


void led_init(void)
{ 

	GPIO_InitTypeDef gpio_init_struct;
    LED2_GPIO_CLK_ENABLE();                                 /* LED2时钟使能 */

    gpio_init_struct.Pin   = LED2_GPIO_PIN;                 /* LED2引脚 */
    gpio_init_struct.Mode  = GPIO_MODE_OUTPUT_PP;           /* 推挽输出 */
    gpio_init_struct.Pull  = GPIO_PULLUP;                   /* 上拉 */
    gpio_init_struct.Speed = GPIO_SPEED_FREQ_HIGH;          /* 高速 */
    HAL_GPIO_Init(LED2_GPIO_PORT, &gpio_init_struct);       /* 初始化LED0引脚 */

    LED2(1);                                                /* 关闭 LED0 */
	
}


