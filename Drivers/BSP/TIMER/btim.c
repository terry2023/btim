#include "./BSPINIT/bspinit.h"

/**
 * @brief       基本定时器TIMX定时中断初始化函数
 * @note
 *              基本定时器的时钟来自APB1,当PPRE1 ≥ 2分频的时候
 *              基本定时器的时钟为APB1时钟的2倍, 而APB1为36M, 所以定时器时钟 = 72Mhz
 *              定时器溢出时间计算方法: Tout = ((arr + 1) * (psc + 1)) / Ft us.
 *              Ft=定时器工作频率,单位:Mhz
 *
 * @param       arr: 自动重装值。
 * @param       psc: 时钟预分频数
 * @retval      无
 */
 
TIM_HandleTypeDef b_timx_handle;  /* 定时器句柄 */

void btim_timx_int_init(uint16_t arr, uint16_t psc)
{
    b_timx_handle.Instance = BTIM_TIMX_INT;                      /* 通用定时器X */
    b_timx_handle.Init.Prescaler = psc;                          /* 设置预分频系数 */
    b_timx_handle.Init.Period = arr;                             /* 自动装载值 */
    b_timx_handle.Init.CounterMode = TIM_COUNTERMODE_UP;         /* 递增计数模式 */
	
    HAL_TIM_Base_Init(&b_timx_handle);
    HAL_TIM_Base_Start_IT(&b_timx_handle);    /* 使能定时器x及其更新中断 */
}

/**
 * @brief       定时器底层驱动，开启时钟，设置中断优先级
                此函数会被HAL_TIM_Base_Init()函数调用
 * @param       htim:定时器句柄
 * @retval      无
 */
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == BTIM_TIMX_INT)
    {
        BTIM_TIMX_INT_CLK_ENABLE();                     /* 使能TIM时钟 */
        HAL_NVIC_SetPriority(BTIM_TIMX_INT_IRQn, 1, 3); /* 抢占1，子优先级3，组2 */
        HAL_NVIC_EnableIRQ(BTIM_TIMX_INT_IRQn);         /* 开启ITM3中断 */
    }
}

/**
 * @brief       定时器TIMX中断服务函数
 * @param       无
 * @retval      无
 */
void BTIM_TIMX_INT_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&b_timx_handle); /* 定时器中断公共处理函数 */
}

/**
 * @brief       定时器更新中断回调函数
 * @param       htim:定时器句柄
 * @retval      无
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == BTIM_TIMX_INT)/* BTIM_TIMX_INT 为 TIM6 */
    {
		if(key_scan())									 /* 如果返回值为1代表按下，led进行翻转（取反） */
        {
            LED2_TOGGLE();
			printf("The key PC9 is pressed\r\nLED state change\r\n");
        }
        else											 /* 否则延时10ms，再次while判断 */
        {
    		if(g_usart_rx_sta == 1)
    		{
    			printf("You entered the number ： ");
    			HAL_UART_Transmit(&g_uart1_handle, (uint8_t*)g_rx_buffer, 1, 1000);
    			while(__HAL_UART_GET_FLAG(&g_uart1_handle, UART_FLAG_TC) != 1);
    			printf("\r\n\r\n");
    			g_usart_rx_sta = 0;
    			printf("Waiting for the key status again………\r\n");
    		}
        }
	}
} 




