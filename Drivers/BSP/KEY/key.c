#include "./BSP/KEY/key.h"
#include "./SYSTEM/delay/delay.h"

/**
 * @brief       按键初始化函数
 * @param       无
 * @retval      无
 */
void key_init(void)
{
    GPIO_InitTypeDef gpio_init_struct;
    KEY9_GPIO_CLK_ENABLE();                                     /* KEY9时钟使能 */

    gpio_init_struct.Pin = KEY9_GPIO_PIN;                       /* KEY9引脚 */
    gpio_init_struct.Mode = GPIO_MODE_INPUT;                    /* 输入 */
    gpio_init_struct.Pull = GPIO_PULLUP;                        /* 上拉 */
    gpio_init_struct.Speed = GPIO_SPEED_FREQ_HIGH;              /* 高速 */
    HAL_GPIO_Init(KEY9_GPIO_PORT, &gpio_init_struct);           /* KEY9引脚模式设置,上拉输入 */
}


/* 按键扫描函数 */
uint8_t key_scan(void)
{
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9) == 0)			/* 读函数，0 低电平代表按下 */
    {
        delay_ms(10);   /* 延迟10ms 进行消抖 */
        if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9) == 0)		/* 再次判断 */
        {
            while(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9) == 0);  /* 循环等待按键松开，若松开再次进行上方10ms消抖 */
            return 1;   /* 按键按下了 */
        }
    }
	/* while循环跳出，不再判断 */
    return 0;   /* 按键抬起 */
}

















