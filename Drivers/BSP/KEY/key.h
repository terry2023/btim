#ifndef __KEY_H
#define __KEY_H

#include "./SYSTEM/sys/sys.h"

/******************************************************************************************/
/* 引脚 定义 */

#define KEY9_GPIO_PORT                  GPIOC
#define KEY9_GPIO_PIN                   GPIO_PIN_9
#define KEY9_GPIO_CLK_ENABLE()          do{ __HAL_RCC_GPIOC_CLK_ENABLE(); }while(0)   /* PE口时钟使能 */

/******************************************************************************************/

#define KEY9        HAL_GPIO_ReadPin(KEY9_GPIO_PORT, KEY9_GPIO_PIN)     /* 读取KEY9引脚 */

void key_init(void);                /* 按键初始化函数 */
uint8_t key_scan(void);     		/* 按键扫描函数 */

#endif


















