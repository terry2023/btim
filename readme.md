

# 这是一个集led/key/btim/gtim/beep/的样板例程



## 基础定时器实验







**keil软件按照“标准文件排版形式”进行排版**

startup_stm32f103xe.s等文件，需要根据芯片型号进行选择。

rtc6内存为256选择xe

具体可见命名规则：https://blog.csdn.net/kunyus/article/details/97136379

**x6适用于16K<=FLASH≤32K小容量 产品；**

**xb适用于64K≤FLASH≤128K中等容量产品；**

**xe适用于256K≤FLASH<=512k大容量产品；**

**xg适用于768K≤FLASH<=1024K超大容量产品**

魔法棒设置：
1、设备查看：芯片型号选择
2、Target    ：编译5版本
3、Output   ：将Project的两个文件夹删除，将此设置到Output文件夹，Create HEX打勾，Browse Information                取消打勾
4、Listing    ：将此设置到Output文件夹
5、C、C++   ：设置全局宏定义：USE_HAL_DRIVER,STM32F103xB       中间使用英文逗号隔开。选择 -O0 优化等级。设置 C99模式。

设置头文件包含路径：（4个Drivers文件夹下，一个User文件夹下）

​							\Drivers\CMISS\Drivers\ST\STM32F1xx\include

​		    	    		\Drivers\STM32F1xx_HAL_Driver\Inc

​							\Drivers\CMISS\Include

​							\Drivers

​							\Users

​	

6、Debug：设置JLink为SW模式