#include "./BSPINIT/bspinit.h"

int main(void)
{
	bspinit();								/* 各外设初始化 */
	LED2(0);								/* 打开LED */
	delay_ms(800);
	LED2_TOGGLE();
	delay_ms(800);
	LED2_TOGGLE();

	printf("Initialization of all peripherals completed\r\n\r\n");
	printf("Waiting for key status………\r\n");
	while(1)
	{ 
		delay_ms(100);
		
	}
}
	
