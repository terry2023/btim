#ifndef _BSPINIT_H
#define _BSPINIT_H

#include "./SYSTEM/sys/sys.h"
#include "./SYSTEM/delay/delay.h"
#include "./SYSTEM/usart/usart.h"
#include "./BSP/LED/led.h"
#include "./BSP/KEY/key.h"
#include "./BSP/TIMER/btim.h"

/******************************************************************************************/
/* 外部接口函数*/
void bspinit(void);                 /* 初始化 */

#endif
