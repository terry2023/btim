#include "./BSPINIT/bspinit.h"

void bspinit(void)
{
	HAL_Init();                                 /* 初始化HAL库 */
    sys_stm32_clock_init(RCC_PLL_MUL9);         /* 设置时钟,72M */
    delay_init(72);                             /* 初始化延时函数 */
    led_init();                                 /* 初始化LED */
	key_init();                                 /* 初始化KEY */
    usart_init(115200);                         /* 波特率设为115200 */
	btim_timx_int_init(1000-1,7200-1);			/* 初始化基础定时器TIM6，5000 7000设置具体看TIMER学习笔记 */

	
}
